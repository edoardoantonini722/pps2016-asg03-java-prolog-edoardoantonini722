package u07asg;


import javax.swing.*;
import java.awt.*;
import java.util.Optional;

import static u07asg.Game.COLS;
import static u07asg.Game.ROWS;

/** A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private final TicTacToe ttt;
    private final JButton[][] board = new JButton[ROWS][COLS];
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("TTT");
    private boolean finished = false;
    private Player turn = Player.getStartingPlayer();
    private int moves = 0;

    private void changeTurn(){
        turn = turn.next();
    }

    public TicTacToeApp(TicTacToe ttt) throws Exception {
        this.ttt=ttt;
        initPane();
    }
    
    private void humanMove(int i, int j){
        if (ttt.move(turn,i,j)){
            board[i][j].setText(this.turn.getSymbol());
            moves++;
            changeTurn();
        }
        Optional<Player> lose = ttt.checkLose();
        if (lose.isPresent()){
            exit.setText(lose.get()+" loses!");
            finished=true;
            return;
        }
        if (ttt.checkCompleted()){
            exit.setText("Even!");
            finished=true;
            return;
        }

        if (moves > 19){
            System.out.println("X not losing finals: "+(ttt.notLosingFinalCount(turn, Player.PlayerX))  + " intermediate not losing boards " + ttt.notLosingStepCount(turn, Player.PlayerX));
            System.out.println("O not losing finals: "+(ttt.notLosingFinalCount(turn, Player.PlayerO))  + " intermediate not losing boards " + ttt.notLosingStepCount(turn, Player.PlayerO));
            System.out.println("~ not losing finals: "+(ttt.notLosingFinalCount(turn, Player.PlayerTilde))  + " intermediate not losing boards " + ttt.notLosingStepCount(turn, Player.PlayerTilde));
        }
    }

    private void initPane(){
        frame.setLayout(new BorderLayout());
        JPanel b=new JPanel(new GridLayout(ROWS,COLS));
        for (int i = 0; i< ROWS; i++){
            for (int j = 0; j< COLS; j++){
                final int i2 = i;
                final int j2 = j;
                board[i][j]=new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> { if (!finished) humanMove(i2,j2); });
            }
        }
        JPanel s=new JPanel(new FlowLayout());
        s.add(exit);
        exit.addActionListener(e -> System.exit(0));
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(400,430);
        frame.setVisible(true);
    }

    public static void main(String[] args){
        new StartingGUI();
    }
}
