package u07asg;

import java.util.List;
import java.util.Optional;

enum Player {
    PlayerX("X"), PlayerO("O"), PlayerTilde("~");

    private String symbol;
    private static Player current = PlayerX;
    Player(String symbol){
        this.symbol = symbol;
    }

    public static void setStartingPlayer(Player player){
        current = player;
    }

    public static Player getStartingPlayer(){
        return current;
    }

    public Player next(){
        if(current == Player.PlayerX){
            current = Player.PlayerO;
        }else if(current == Player.PlayerO){
            current = Player.PlayerTilde;
        }else if(current == Player.PlayerTilde){
            current = Player.PlayerX;
        }
        return current;
    }

    public String getSymbol() {
        return symbol;
    }
}


public interface TicTacToe {

    void createBoard();

    List<Optional<Player>> getBoard();

    boolean checkCompleted();

    Optional<Player> checkLose();

    boolean move(Player player, int i, int j);

    int notLosingFinalCount(Player current, Player winner);

    int notLosingStepCount(Player current, Player winner);
}
