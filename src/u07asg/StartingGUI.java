package u07asg;

import javax.swing.*;
import java.awt.*;

public class StartingGUI {
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("Choose first player");

    public StartingGUI(){
        initPane();
    }

    private void initPane(){
        JPanel panel = new JPanel(new GridLayout(1,3));
        JButton xButton = new JButton("X");
        JButton oButton = new JButton("O");
        JButton tildeButton = new JButton("~");
        xButton.addActionListener(e -> { Player.setStartingPlayer(Player.PlayerX); close(); });
        oButton.addActionListener(e -> { Player.setStartingPlayer(Player.PlayerO); close(); });
        tildeButton.addActionListener(e -> { Player.setStartingPlayer(Player.PlayerTilde); close(); });

        panel.add(xButton);
        panel.add(oButton);
        panel.add(tildeButton);

        frame.add(panel);
        frame.setSize(400,200);
        frame.setVisible(true);

    }

    private void close(){
           frame.dispose();
            try {
                new TicTacToeApp(new TicTacToeImpl("src/u07asg/3plMisere5X5TTT.pl"));
            } catch (Exception e) {
                System.out.println("Problems loading the theory");
            }
    }
}
